#!/bin/bash

SWAGGER_YAML=$(mktemp).yaml

docker run ioggstream/api-spec-converter \
	-f openapi_3 \
	-t swagger_2 \
	-s yaml \
	https://gitlab.com/istio-hackathon/api-spec/raw/master/openapi.yaml \
	> $SWAGGER_YAML

swagger generate server \
	--spec=$SWAGGER_YAML \
	-A api \
	--model-package=lib/model \
	--server-package=lib/server \
	-C $( dirname ${BASH_SOURCE[0]} )/templates/server.yaml

rm -f $SWAGGER_YAML