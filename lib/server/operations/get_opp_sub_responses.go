// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	model "gitlab.com/istio-hackathon/api/lib/model"
)

// GetOppSubOKCode is the HTTP code returned for type GetOppSubOK
const GetOppSubOKCode int = 200

/*GetOppSubOK Result of Operation

swagger:response getOppSubOK
*/
type GetOppSubOK struct {

	/*
	  In: Body
	*/
	Payload *model.OperationResponse `json:"body,omitempty"`
}

// NewGetOppSubOK creates GetOppSubOK with default headers values
func NewGetOppSubOK() *GetOppSubOK {

	return &GetOppSubOK{}
}

// WithPayload adds the payload to the get opp sub o k response
func (o *GetOppSubOK) WithPayload(payload *model.OperationResponse) *GetOppSubOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get opp sub o k response
func (o *GetOppSubOK) SetPayload(payload *model.OperationResponse) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetOppSubOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
