// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// GetStoreHandlerFunc turns a function with the right signature into a get store handler
type GetStoreHandlerFunc func(GetStoreParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetStoreHandlerFunc) Handle(params GetStoreParams) middleware.Responder {
	return fn(params)
}

// GetStoreHandler interface for that can handle valid get store params
type GetStoreHandler interface {
	Handle(GetStoreParams) middleware.Responder
}

// NewGetStore creates a new http.Handler for the get store operation
func NewGetStore(ctx *middleware.Context, handler GetStoreHandler) *GetStore {
	return &GetStore{Context: ctx, Handler: handler}
}

/*GetStore swagger:route GET /store/{id} getStore

GetStore get store API

*/
type GetStore struct {
	Context *middleware.Context
	Handler GetStoreHandler
}

func (o *GetStore) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewGetStoreParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
