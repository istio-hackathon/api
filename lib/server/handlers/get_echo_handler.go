package handlers

import (
	"os"
	"fmt"
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/istio-hackathon/api/lib/server/operations"
	"gitlab.com/istio-hackathon/api/lib/model"
)

func GetEchoHandler(params operations.GetEchoParams) middleware.Responder {
	echo_uri := fmt.Sprintf("http://%s:%s/echo", os.Getenv("ECHO_SERVICE_HOST"), os.Getenv("ECHO_SERVICE_PORT"))

	req_object := model.EchoResponse{Value: params.Value}
	res_object := model.EchoResponse{}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(req_object)
	res, _ := http.Post(echo_uri, "application/json", b)
	json.NewDecoder(res.Body).Decode(&res_object)

	return operations.NewGetEchoOK().WithPayload(&res_object)
}